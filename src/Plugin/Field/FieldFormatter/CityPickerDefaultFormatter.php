<?php

namespace Drupal\city_picker\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal;

/**
 * Plugin implementation of the 'CityPickerDefaultFormatter' formatter.
 *
 * @FieldFormatter(
 *   id = "CityPickerDefaultFormatter",
 *   label = @Translation("CityPicker"),
 *   field_types = {
 *     "CityPicker"
 *   }
 * )
 */
class CityPickerDefaultFormatter extends FormatterBase {

  /**
   * Define how the field type is showed.
   * 
   * Inside this method we can customize how the field is displayed inside 
   * pages.
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $elements = [];
    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#type' => 'markup',
        '#markup' => $item->city . $item->street
      ];
    }

    return $elements;
  }
  
} // class
