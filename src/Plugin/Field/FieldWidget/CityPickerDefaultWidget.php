<?php

namespace Drupal\city_picker\Plugin\Field\FieldWidget;

use Drupal;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'CityPickerDefaultWidget' widget.
 *
 * @FieldWidget(
 *   id = "CityPickerDefaultWidget",
 *   label = @Translation("CityPicker select"),
 *   field_types = {
 *     "CityPicker"
 *   }
 * )
 */
class CityPickerDefaultWidget extends WidgetBase {

  /**
   * Define the form for the field type.
   * 
   * Inside this method we can define the form used to edit the field type.
   * 
   * Here there is a list of allowed element types: https://goo.gl/XVd4tA
   */
  public function formElement(
    FieldItemListInterface $items,
    $delta, 
    Array $element, 
    Array &$form, 
    FormStateInterface $formState
  ) {

    // City

    $element['city'] = [
      '#type' => 'textfield',
      '#title' => t('城镇'),
	  '#theme_wrappers' => ['form_element', 'city_picker-element'],
      '#default_value' => isset($items[$delta]->city) ? $items[$delta]->city : null,
      '#empty_value' => '',
      '#placeholder' => t('请选择城镇'),
	  '#attributes'=> array('data-toggle' => 'city-picker'),
	  '#attached' => ['library' => ['city_picker/drupal.citypicker'],],
    ];
    // Street

    $element['street'] = [
      '#type' => 'textfield',
      '#title' => t('街区'),

      // Set here the current value for this field, or a default value (or 
      // null) if there is no a value
      '#default_value' => isset($items[$delta]->street) ? 
          $items[$delta]->street : null,

      '#empty_value' => '',
      '#placeholder' => t('请输入街区'),
	  
    ];
    return $element;
  }

} // class
